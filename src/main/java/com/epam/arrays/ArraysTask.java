package com.epam.arrays;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ArraysTask {
  private int firstarray[] = {1, 2, 3, 4, 5};
  private int secondarray[] = {4, 5, 6, 7, 8};

  public int[] formThirdArray() {
    int[] thirdarray = new int[this.firstarray.length + this.secondarray.length];
    System.out.println(Arrays.toString(thirdarray));
    for (int j = 0; j < firstarray.length; j++) {
      thirdarray[j] = firstarray[j];
    }
    for (int i = 0; i < secondarray.length; i++) {
      thirdarray[i + 5] = secondarray[i];
    }
    System.out.println(Arrays.toString(thirdarray));
    return thirdarray;
  }

  public int[] formUniqueArray() {
    int[] uniquearray = new int[this.firstarray.length + this.secondarray.length];

    for (int i = 0; i < firstarray.length; i++) {
      int number = this.firstarray[i];
      if (!determineUnique(number,secondarray)) {
        uniquearray[i] = number;
      }
    }

    for (int i = 0; i < secondarray.length; i++) {
      int numbertwo = this.secondarray[i];
      if(!determineUnique(numbertwo,firstarray)){
        uniquearray[i+firstarray.length] = numbertwo;
      }
    }

    System.out.println(Arrays.toString(uniquearray)+" My unique");
    return uniquearray;
  }

  private boolean determineUnique(int number,int[] array) {

    for (int i = 0; i < array.length; i++) {
      if (number == array[i]) {
        return true;
      }
    }
    return false;
  }

  public Object[] deleteDuplicates(){
    Integer[] set = {1,2,2,3,4,4,5,6,7,8,9,9};
    List<Integer> list = Arrays.asList(set);
    HashSet<Integer> integers = new HashSet<Integer>(list);
    return integers.toArray();
  }
}
