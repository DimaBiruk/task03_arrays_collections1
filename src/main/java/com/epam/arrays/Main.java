package com.epam.arrays;

import java.util.Arrays;

public class Main {

  public static void main(String[] args) {
    ArraysTask arrays = new ArraysTask();
    arrays.formThirdArray();
    arrays.formUniqueArray();
    System.out.println(Arrays.toString(arrays.deleteDuplicates())+" Deleted");
  }

}
